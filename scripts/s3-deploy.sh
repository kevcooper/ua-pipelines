#!/bin/bash

DEPLOY_DIR=$1
BUCKET_NAME=$2

apt-get install -y python-dev

curl https://bootstrap.pypa.io/get-pip.py > /tmp/get-pip.py

python /tmp/get-pip.py
pip install awscli

aws s3 sync --delete $DEPLOY_DIR s3://$BUCKET_NAME

